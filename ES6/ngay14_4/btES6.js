//bai9
class Product {
        constructor Product(id, name, categoryId, saleDate, qulity, isDelete){
            this.id = id;
            this.name = name;
            this.categoryId = categoryId;
            this.saleDate = saleDate;
            this.qulity = qulity;
            this.isDelete = isDelete;

        }
    } 
    const idProduct = new Product();
    const nameProduct = new Product();
    const categoryIdProduct = new Product();
    const saleDateProduct = new Product();
    const qulityProduct = new Product();
const isDeteteProduct = new Product();
    
//bai10
function listproduct(){
    const pushlistproduct = [];
    let main1 = new product(1, 'Táo ', 1, new Date(2022, 4, 14), 0, true);
    pushlistproduct.push(main1);

    let main2 = new product(2, 'Cam ', 1, new Date(2022, 4, 14), 2, true);
    pushlistproduct.push(main2);

    let main3 = new product(3, 'Chanh ', 1, new Date(2022, 4, 14), 0, true);
    pushlistproduct.push(main3);

    let main4 = new product(4, 'Bưởi ', 3, new Date(2022, 4, 14), 1, false);
    pushlistproduct.push(main4);

    let main5 = new product(5, 'Xoài ', 1, new Date(2022, 4, 14), 2, false);
    pushlistproduct.push(main5);

    let main6 = new product(6, 'Quất ', 2, new Date(2022, 4, 14), 0, true);
    pushlistproduct.push(main6);

    let main7 = new product(7, 'Nhãn ', 1, new Date(2022, 4, 14), 3, false);
    pushlistproduct.push(main7);

    let main8 = new product(8, 'Hồng ', 3, new Date(2022, 4, 14), 1, true);
    pushlistproduct.push(main8);

    let main9 = new product(9, 'Mơ ', 1, new Date(2022, 4, 14), 0, true);
    pushlistproduct.push(main9);

    let main10 = new product(10, 'Mận ', 1, new Date(2022, 4, 14), 0, true);
    pushlistproduct.push(main10);

    return pushlistproduct;
}
const listProduct = listproduct();
console.log(listProduct);

// Bài 11
// ES6
function fiterProductById(listProduct, idProduct){
    const productname = listProduct.find(product => product.id === idProduct);
    return productname;
}
console.log(fiterProductById(listProduct,1));

// Dùng vòng For
function fiterProductByIdFor(listProduct, idProduct){
    for (let product of listProduct){
        if(product.id == idProduct){
            return product
        }
    }
    return false
}

const checkID = fiterProductByIdFor(listProduct, 10);
if (checkID != false){
    console.log(checkID);
}else {
    console.log('Không tồn tại');
}

// Bài 12
// ES6
function fiterProductByQuility(listProduct) {
    const fiterProduct = listProduct.filter(product => product.qulity > 0 && product.isDelete === false )
    return fiterProduct;
}

console.log(fiterProductByQuility(listProduct))

// Dùng for
function fiterProductByQulityFor(listProduct){
    const pushlistproduct = [];
    for (let product of listProduct){
        if(product.qulity > 0 && product.isdelete === false){
            pushlistproduct.push(product);
        }
    }
    if(!pushlistproduct.length){
        return false
    }
    return pushlistproduct
}
const checklistproductfor = fiterProductByQulityFor(listProduct);
if (checklistproductfor !== false){
    console.log(checklistproductfor);
}else {
    console.log('Không có giá trị nào hợp lệ');
}


// Bài 13
// ES6
function fiterProductBySaleDate(listProduct) {
    var newDate = new Date()
    const fiterProduct = listProduct.filter(product => product.saleDate - newDate.getDate() > 0 && product.isDelete === false )
    return fiterProduct
}

console.log(fiterProductBySaleDate(listProduct))
// Dùng for
function fiterProductBySaleDateFor(listProduct) {
    var newDate2 = new Date()
    const pushlistproduct2 = []
    for (let product of listProduct) {
        if(product.isDelete === false && product.saleDate  - newDate2.getDate() > 0) {
            pushlistproduct2.push(product);
        }
    }
    if(!pushlistproduct2.length) {
        return false
    }
    return pushlistproduct2
}
const checklistproductfor2 = fiterProductBySaleDateFor(listProduct);
if(checklistproductfor2 !== false) {
    console.log(checklistproductfor2)
}else {
    console.log('Rỗng')
}

// Bài 14
// Reduce
function filterProductReduce(listProduct) {
    const filterProduct3 = listProduct.filter(product => product.isDelete === false )
    const totalQuility = filterProduct3.reduce((total, num) => {
        return total + num.qulity
    },0);
    return totalQuility
}

console.log(filterProductReduce(listProduct))
// Không Reduce

function filterProductReduce2(listProduct) {
    var listproduct4 = []
    for(let product of listProduct) {
        if(product.isDelete === false) {
            listproduct4.push.qulity
        }
        if(!listproduct4.length) {
            return false
        }
        return listproduct4
    }
    const checklistproductfor4 = filterProductReduce2(listProduct);
    if(checklistproductfor4 != false) {
        console.log(checklistproductfor4)
    }else {
        console.log('Rỗng')
    }
}
// Bài 15
// ES6
function isHaveProductInCategory(listProduct,categoryId) {
    const categoryid = listProduct.filter(product => product.categoryId === categoryId)
    if(categoryid) {
        return true
    }
    return false
}

console.log(isHaveProductInCategory(listProduct, 3))
// Dùng for
function isHaveProductInCategory2(listProduct,categoryid) {
    for (let product of listProduct ) {
        if (product.categoryId === categoryid){
            return true
        }
      
    }
    return false
}

console.log(isHaveProductInCategory2(listProduct, 3))
// Bài 16
// ES6
function fiterProductSaleDateQulity(listProduct) {
    var nowDate = new Date()
    const filterProduct2 = listProduct.filter(product => product.saleDate - nowDate.getDate() > 0 && product.qulity > 0  )
    const listreduceproduct = filterProduct2.map((item) => {
        return {id: item.id, name:item.name}
    });
    return listreduceproduct
}

console.log(fiterProductSaleDateQulity(listProduct))
// Dùng for
function fiterProductSaleDateQulity2(listProduct) {
    var listProduct3 = []
    var nowDate2 = new Date()
    for(let product of listProduct) {
        if(product.saleDate - nowDate2.getDate() > 0 && product.qulity > 0) {
            listProduct3.push(product.id, product.name)
        }
    }
    if(!listProduct3.length){
        return false
    }
    return listProduct3
}   
const checklistproductfor3 = fiterProductSaleDateQulity2(listProduct);
if(checklistproductfor3 !== false) {
    console.log(checklistproductfor3)
}else {
    console.log('Rỗng')
}