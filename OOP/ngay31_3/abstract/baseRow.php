<?php
require('../interface/interfaceEntity.php');
abstract class BaseRow implements IEntity
{
    protected $id;
    protected $name;

    /**
     * Get Id by Row
     * @return int
     */ 
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set Id for Row
     * @return void
     */
    public function setId($id)
    {
        return $this->id = $id;
    }
}
