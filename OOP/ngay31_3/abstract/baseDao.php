<?php
require('../dao/database.php');
require('../interface/interfaceDao.php');

abstract class BaseDao implements IDao
{
    protected $database;

    public function __construct()
    {
        $this->database = DataBase::getInstants();
    }

    /**
     * Get name table from param row
     * 
     * @param $row
     * @return string
     */
    public function getTableName($row)
    {
        return strtolower(get_class($row))  . 'Table';
    }

    /**
     * Insert row to table
     * 
     * @param $row
     * @return void
     */
    public function insert($row)
    {
        return $this->database->insertTable($this->getTableName($row), $row);
    }

    /**
     * select row from table
     * 
     * @param $row
     * @return void
     */
    public function select($row)
    {
        return $this->database->selectTable($this->getTableName($row));
    }

    /**
     * update row to table
     * 
     * @param $row
     * @return void
     */
    public function update($row)
    {
        return $this->database->updateTable($this->getTableName($row), $row);
    }

    /**
     * Delete row to table
     * 
     * @param $row
     * @return void
     */
    public function delete($row)
    {
        return $this->database->deleteTable($this->getTableName($row), $row);
    }
}
