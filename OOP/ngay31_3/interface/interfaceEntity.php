<?php

interface IEntity
{
    /**
     * Get Id from Object
     */
    public function getId();

    /**
     * Set Id to Object
     */
    public function setId($id);
}
