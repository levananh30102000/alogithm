<?php

interface IDao
{
    /**
     * Insert row to Table
     * @param object $row
     * @return void
     */
    public function insert(object $row);
}
