<?php
require("../entity/Product.php");

class ProductDemo extends Product
{
    public function __construct($id, $name, $categoryId)
    {
        parent::__construct($id, $name, $categoryId);
    }

    /**
     * Test create product
     * @param $id
     * @param $name
     * @param $categoryId
     * @return object
     */
    public function createProductTest($id, $name, $categoryId)
    {
        $product = new ProductDemo($id, $name, $categoryId);
        return $product;
    }

    /**
     * Test print product
     * @param $product
     * @return array product
     */
    public function printProduct($product)
    {
        return $product;
    }
}

$newProduct = new ProductDemo(1, "anh", 2);
print_r($newProduct->printProduct($newProduct));




