<?php
require("../dao/CategoryDAO.php");
class CategoryDaoDemo extends Database
{
    /**
     * Test insert row to Category table
     * 
     * @param $row
     * @return array
     */
    function insertTest($row)
    {
        print_r($this->insertTable($row));
    }

    /**
     * Test select row from category table
     * @param $row
     * @return array
     */
    public function selectTest($row)
    {
        print_r($this->selectTable($row));
    }

    /**
     * Test update row to category table
     * 
     * @param $row
     * @return array
     */
    public function updateTest($row)
    {
        print_r($this->updateTable($row));
    }

    /**
     * Test delete row to category table
     * 
     * @param $row
     * @return array
     */
    public function deleteTest($row)
    {
        print_r($this->deleteTable($row));
    }
}

$category = new Category(2, 'điện thoại');
$category1 = new Category(2, 'lap top');
$categoryDaoDemo = new CategoryDaoDemo();
$categoryDaoDemo->insertTest($category);
$categoryDaoDemo->selectTest($category);
$categoryDaoDemo->updateTest($category1);
$categoryDaoDemo->deleteTest($category1);
