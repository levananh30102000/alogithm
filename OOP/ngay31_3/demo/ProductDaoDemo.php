<?php
require ("../dao/ProductDao.php");

class ProductDaoDemo extends ProductDao
{
    /**
     * Test insert row to product table
     * 
     * @param $row
     * @return array
     */
    public function insertTest($row)
    {
        $this->insert($row);
    }

    /**
     * Test select row from product table
     * 
     * @param $row
     * @return array
     */
    public function selectTest($row)
    {
        print_r($this->select($row));
    }

    /**
     * Test update row to product table
     * 
     * @param $row
     * @return array
     */
    public function updateTest($row)
    {
        print_r($this->update($row));
    }

    /**
     * Test delete row to product table
     * 
     * @param $row
     * @return array
     */
    public function deleteTest($row)
    {
        print_r($this->delete($row));
    }
}

$productDaoDemo = new ProductDaoDemo();
$product = new Product(1, 'samsung A9', 2);
$productDaoDemo->insertTest($product);
$productDaoDemo->selectTest($product);

