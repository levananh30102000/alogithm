<?php
require("../dao/AccessoryDAO.php");
class AccessoryDaoDemo extends AccessoryDao
{
    /**
     * Test insert row to accessory table
     * 
     * @param $row
     * @return array
     */
    public function insertTest($row)
    {
        $this->insert($row);
    }

    /**
     * Test select row from accessory table
     * 
     * @param $row
     * @return array
     */
    public function selectTest($row)
    {
        print_r($this->select($row));
    }

    /**
     * Test update row to accessory table
     * 
     * @param $row
     * @return array
     */
    public function updateTest($row)
    {
        print_r($this->update($row));
    }

    /**
     * Test delete row to accessory table
     * 
     * @param $row
     * @return array
     */
    public function deleteTest($row)
    {
        print_r($this->delete($row));
    }
}

$accessoryDaoDemo = new AccessoryDaoDemo();
$accessory = new Accessory(1, 'Chip A9', 'vượt trội');
$accessory2 = new Accessory(1, 'Chip A8', 'vượt trội');
$accessoryDaoDemo->insertTest($accessory);
$accessoryDaoDemo->selectTest($accessory);
$accessoryDaoDemo->updateTest($accessory2);
$accessoryDaoDemo->deleteTest($accessory);
