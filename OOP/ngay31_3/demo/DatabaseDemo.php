<?php
require("../dao/Database.php");
require("../entity/Product.php");
class DatabaseDemo extends Database 
{
    /**
     * Test insert row to table
     */
    public function insertTableTest()
    {
        $row = new Product(1, 'anh', 2);
        $row2 = new Product(2, 'thuan', 1);
        print_r($this->insertTable('productTable', $row));
        print_r($this->insertTable('productTable', $row2));
    }

    /**
     * Test select row from tatble
     */
    public function selectTableTest()
    {
        print_r($this->selectTable('productTable'));
    }

    /**
     *  Test update row to table
     */
    public function updateTableTest()
    {
        $row = new Product(1, 'leanh', 2);
        print_r($this->updateTable('productTable', $row));

    }

    /**
     * Test delete row for table
     */
    public function deleteTableTest()
    {
        $row = new Product(1, 'leanh', 2);
        print_r($this->deleteTable('productTable', $row)); 
    }

    /**
     * Test truncate Table
     */
    public function truncateTableTest()
    {
        print_r($this->truncateTable('productTable'));
    }

    /**
     * Test update to table by id
     */
    public function updateTableByIdTest($id, $row)
    {
        
        $row = new Product(2, 'hoathuan', 2);
        print_r($this->updateTableById('productTable',1, $row));
    }

}


$demoDataBase = new DataBaseDemo();
$demoDataBase->insertTableTest();
$demoDataBase->selectTableTest();
$demoDataBase->updateTableTest();
$demoDataBase->deleteTableTest();



