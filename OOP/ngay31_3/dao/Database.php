<?php
class Database 
{
    public $productTable = array();
    public $categoryTable = array();
    public $accessoryTable = array();
    public $instants = null;

    /**
     * Get database to instant
     * 
     * @param $classData
     * @return $instant
     */
    public static function getInstants()
    {
        if(empty(self::$instants))
        {
            self::$instants = new DataBase('DataBase');
        }
        return self::$instants;
    }


    /**
     * Insert row to table
     * @param $nameTable
     * @param $row
     * @return array
     */
    public function insertTable($nameTable, $row)
    {
        $this->$nameTable[] = $row;
        return $this->$nameTable;
    }

    /**
     * SelectTable with nameTable
     * @param $nameTable
     * @return array
     */
    public function selectTable($nameTable, $id = null)
    {
        if(isset($nameTable))
        {
            return $this->{$nameTable};
        }
    }

    /**
     * Update row to table
     * @param $nameTable
     * @param $row
     * @return array
     */
    public function updateTable($nameTable,$row)
    {
        foreach ($this->$nameTable as $key => $entityRow)
        {
            if($this->$nameTable[$key]->getId() == $row->getId())
            {
                $this->$nameTable[$key] == $row;
            }
        }
        return $this->$nameTable;
    }

    /**
     * Delete row to table
     * @param $nameTable
     * @param $row
     * @return array
     */
    public function deleteTable($nameTable, $row)
    {
        foreach ($this->$nameTable as $key => $entityRow)
        {
            if($this->$nameTable[$key]->getId() == $row->getId())
            {
                unset ($this->{$nameTable}[$key]);
                $this->{$nameTable}[$key] = [];
            }
        }
        return $this->$nameTable;
    }

    /**
     * Truncate to table
     * @param $nameTable
     * @return array 
     */
    public function truncateTable($nameTable) {
        if ($this->{$nameTable} == $nameTable)
        {
            unset($this->{$nameTable});
        }
        return $this->{$nameTable};
    }


    /**
     * Update table by Id
     * @param $id
     * @param $row
     * @return array
     */
    public function updateTableById($nameTable, $id, $row)
    {
            foreach ($this->$nameTable as $key => $entityRow)
            {
                if ($this->{$nameTable}[$key]->getId() == $id)
                {
                    $this->{$nameTable}[$key] = $row;
                }
            }
        return $this->$nameTable;
    }
}