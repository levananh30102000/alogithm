<?php
require("../entity/Product.php");

class Category extends BaseRow
{
    public function __construct($id, $name)
    {
        $this->id = $id;
        $this->name = $name;
    }
}
