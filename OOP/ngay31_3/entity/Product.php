<?php
require('../abstract/baseRow.php');

class Product extends BaseRow
{
    public function __construct($id, $name, $categoryId)
    {
        $this->id = $id;
        $this->name = $name;
        $this->categoryId = $categoryId;
    }
}

