<?php
require('../abstract/baseRow.php');
class Accessory extends BaseRow
{
    public function __construct($id, $name, $detail)
    {
        $this->id = $id;
        $this->name = $name;
        $this->detail = $detail;
    }
}