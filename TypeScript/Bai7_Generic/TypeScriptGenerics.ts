// Kiểu dữ liệu mà có nhận tham số và trả về kiểu dữ liệu tương ứng
interface List1<T>{
    length: number;
    [index: number]: T;
};

const numberList: List1<number> = [1, 2, 3];
const wordList: List1<string> = ['dev', 'BE', 'FE'];


// Vi du 2:
// Phần sau cho thấy một hàm chung trả về phần tử ngẫu nhiên từ một mảng kiểu T:
function getRandomElement<T>(items: T[]): T {
    let randomIndex = Math.floor(Math.random() * items.length);
    return items[randomIndex];
}
let numbers = [1, 5, 7, 4, 2, 9];
let colors = ['red', 'green', 'blue'];

console.log(getRandomElement(numbers));
console.log(getRandomElement(colors));