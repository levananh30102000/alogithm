// Nạp chồng hàm cho phép bạn thiết lập mối quan hệ giữa các kiểu tham số và kiểu kết quả của một hàm
function add(a: number, b: number): number;
function add(a: string, b: string): string;
function add(a: any, b: any): any {
   return a + b;
}

/**
 *  Quá tải hàm với các tham số tùy chọn:
 *  - Khi bạn nạp chồng một hàm, số lượng tham số bắt buộc phải giống nhau. 
 *  Nếu quá tải có nhiều tham số hơn tham số khác, 
 *  bạn phải làm cho các tham số bổ sung là tùy chọn.
 */
function sum(a: number, b: number): number;
function sum(a: number, b: number, c: number): number;
function sum(a: number, b: number, c?: number): number {
    if (c) return a + b + c;
    return a + b;
}
 

/**
 *  Quá tải phương thức
 *  - Khi một hàm là một thuộc tính của một lớp, nó được gọi là một phương thức
 *  - TypeScript cũng hỗ trợ nạp chồng phương thức. 
 */
class Counter {
    private current: number = 0;
    count(): number;
    count(target: number): number[];
    count(target?: number): number | number[] {
        if (target) {
            let values = [];
            for (let start = this.current; start <= target; start++) {
                values.push(start);
            }
            this.current = target;
            return values;
        }
        return ++this.current;
    }
}
let counter = new Counter();

console.log(counter.count()); // return a number
console.log(counter.count(20)); // return an array