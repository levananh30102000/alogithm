- Kiểu anycho phép bạn gán giá trị thuộc bất kỳ kiểu nào cho một biến:
// json may come from a third-party API
const json = `{"latitude": 10.11, "longitude":12.12}`;

// parse JSON to find location
const currentLocation = JSON.parse(json);
console.log(currentLocation);

- Nếu bạn khai báo một biến với objectkiểu, bạn cũng có thể gán cho nó bất kỳ giá trị nào.
- Tuy nhiên, bạn không thể gọi một phương thức trên đó ngay cả khi phương thức đó thực sự tồn tại.
let result: any;
result = 10.123;
console.log(result.toFixed());
result.willExist(); //


