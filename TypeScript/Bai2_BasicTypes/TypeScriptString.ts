- Giống như JavaScript, TypeScript sử dụng dấu ngoặc kép(") hoặc dấu ngoặc đơn ( ') để bao quanh các ký tự chuỗi:
let firstName: string = 'John';
let title: string = "Web Developer";

- Ví dụ sau đây cho thấy cách tạo chuỗi nhiều dòng bằng cách sử dụng backtick(`):
let description = `This TypeScript string can 
span multiple 
lines
`;

-Nội suy chuỗi cho phép bạn nhúng các biến vào chuỗi như sau:
let firstName: string = `John`;
let title: string = `Web Developer`;
let profile: string = `I'm ${firstName}. 
I'm a ${title}`;

console.log(profile);
