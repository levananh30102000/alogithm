- Số thực phẩy dộng 
let price: number;
- Số thập phân
let counter: number = 0;
let x: number = 100, 
    y: number = 200;
- Số nhị phân
let bin = 0b100;
let anotherBin: number = 0B010;
- Số bát phân
let octal: number = 0o10;
- Số thập lục phân
let hexadecimal: number = 0XA;
- Số nguyên lớn
let big: bigint = 9007199254740991n;
