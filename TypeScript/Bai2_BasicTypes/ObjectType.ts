- Sau đây là cách khai báo một biến chứa một đối tượng:
let employee: object;

employee = {
    firstName: 'John',
    lastName: 'Doe',
    age: 25,
    jobTitle: 'Web Developer'
};

console.log(employee);

- Để chỉ định rõ ràng các thuộc tính của employeeđối tượng, trước tiên bạn sử dụng cú pháp sau để khai báo employeeđối tượng:
let employee: {
    firstName: string;
    lastName: string;
    age: number;
    jobTitle: string;
};

- Và sau đó bạn gán employeeđối tượng cho một đối tượng theo nghĩa đen với các thuộc tính được mô tả:Và sau đó bạn gán employeeđối tượng cho một đối tượng theo nghĩa đen với các thuộc tính được mô tả:
employee = {
    firstName: 'John',
    lastName: 'Doe',
    age: 25,
    jobTitle: 'Web Developer'
};

2. Loại trống{ }
- Kiểu trống { }mô tả một đối tượng không có thuộc tính của riêng nó.Nếu bạn cố gắng truy cập một thuộc tính trên đối tượng như vậy, TypeScript sẽ phát ra lỗi thời gian biên dịch:
let vacant: {};
vacant.firstName = 'John';
