- Các kiểu ký tự chuỗi cho phép bạn xác định một kiểu chỉ chấp nhận một ký tự chuỗi được chỉ định.
- Phần sau định nghĩa một kiểu ký tự chuỗi chấp nhận một chuỗi ký tự 'click':
let click: 'click';
- Là clickmột kiểu ký tự chuỗi chỉ chấp nhận chuỗi ký tự 'click'.Nếu bạn gán chuỗi ký tự cho chuỗi ký tự click, chuỗi ký tự clicknày sẽ hợp lệ:
click = 'click'; // valid
- Tuy nhiên, khi bạn gán một chuỗi ký tự khác cho click, trình biên dịch TypeScript sẽ xuất hiện lỗi.Ví dụ:
click = 'dblclick'; // compiler error
- Các kiểu chuỗi ký tự có thể kết hợp độc đáo với các kiểu liên hợp để xác định một tập hợp hữu hạn các giá trị chuỗi ký tự cho một biến:
let mouseEvent: 'click' | 'dblclick' | 'mouseup' | 'mousedown';
mouseEvent = 'click'; // valid
mouseEvent = 'dblclick'; // valid
mouseEvent = 'mouseup'; // valid
mouseEvent = 'mousedown'; // valid
mouseEvent = 'mouseover'; // compiler error

- Nếu bạn sử dụng các kiểu ký tự chuỗi ở nhiều nơi, chúng sẽ rất dài dòng.
+ Để tránh điều này, bạn có thể sử dụng loại bí danh.Ví dụ:
type MouseEvent: 'click' | 'dblclick' | 'mouseup' | 'mousedown';
let mouseEvent: MouseEvent;
mouseEvent = 'click'; // valid
mouseEvent = 'dblclick'; // valid
mouseEvent = 'mouseup'; // valid
mouseEvent = 'mousedown'; // valid
mouseEvent = 'mouseover'; // compiler error

let anotherEvent: MouseEvent;