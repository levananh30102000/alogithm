- Trong TypeScript, một kiểu là một cách thuận tiện để tham chiếu đến các thuộc tính và chức năng khác nhau mà một giá trị có.
- TypeScript kế thừa các kiểu dựng sẵn từ JavaScript. Các loại TypeScript được phân loại thành:
+ Các loại nguyên thủy
+ Các loại đối tượng
- Có hai mục đích chính của các loại trong TypeScript:
+ Đầu tiên, các kiểu được trình biên dịch TypeScript sử dụng để phân tích mã của bạn để tìm lỗi
+ Thứ hai, các kiểu cho phép bạn hiểu những giá trị nào được liên kết với các biến.