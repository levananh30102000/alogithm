- Loại neverlà một loại không chứa giá trị.Do đó, bạn không thể gán bất kỳ giá trị nào cho một biến có neverkiểu.
- Thông thường, bạn sử dụng neverkiểu để đại diện cho kiểu trả về của một hàm luôn tạo ra lỗi. 
function raiseError(message: string): never {
    throw new Error(message);
}
- Ví dụ: không có neverkiểu, hàm sau đây gây ra lỗi vì không phải tất cả các đường dẫn mã đều trả về một giá trị.
function fn(a: string | number): boolean {
    if (typeof a === "string") {
      return true;
    } else if (typeof a === "number") {
      return false;
    }   
}
  
- Để làm cho mã hợp lệ, bạn có thể trả về một hàm có kiểu trả về là neverkiểu.
function fn(a: string | number): boolean {
    if (typeof a === "string") {
      return true;
    } else if (typeof a === "number") {
      return false;
    }  
    // make the function valid
    return neverOccur();
  }
  
  let neverOccur = () => {
     throw new Error('Never!');
  } 