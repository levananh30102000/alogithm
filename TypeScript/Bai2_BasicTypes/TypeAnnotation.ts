- TypeScript sử dụng các chú thích kiểu để chỉ định rõ ràng các kiểu cho các định danh như biến, hàm, đối tượng, v.v.
- TypeScript sử dụng cú pháp: typesau mã định danh làm chú thích kiểu, trong đó typecó thể là bất kỳ kiểu hợp lệ nào.
- Sau khi một số nhận dạng được chú thích với một loại, nó chỉ có thể được sử dụng làm loại đó.Nếu mã định danh được sử dụng như một kiểu khác, trình biên dịch TypeScript sẽ gây ra lỗi.

- Cú pháp sau cho biết cách chỉ định chú thích kiểu cho các biến và hằng số:
let variableName: type;
let variableName: type = value;
const constantName: type = value;

- Để chú thích một kiểu mảng, bạn sử dụng một kiểu cụ thể theo sau là dấu ngoặc vuông: type[]:
let arrayName: type[];

- Để chỉ định kiểu cho một đối tượng, bạn sử dụng chú thích kiểu đối tượng.Ví dụ:
let person: {
    name: string;
    age: number
 };
 
 person = {
    name: 'John',
    age: 25
}; // valid
 
- Phần sau hiển thị chú thích hàm với chú thích kiểu tham số và chú thích kiểu trả về:
let greeting: (name: string) => string;
