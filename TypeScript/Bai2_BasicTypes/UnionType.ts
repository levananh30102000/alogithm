- Đôi khi, bạn sẽ gặp phải một hàm yêu cầu một tham số là một số hoặc một chuỗi.Ví dụ:
function add(a: any, b: any) {
    if (typeof a === 'number' && typeof b === 'number') {
        return a + b;
    }
    if (typeof a === 'string' && typeof b === 'string') {
        return a.concat(b);
    }
    throw new Error('Parameters must be numbers or strings');
}

- Ví dụ, biến sau thuộc loại numberhoặc string:
let result: number | string;
result = 10; // OK
result = 'Hi'; // also OK
result = false; // a boolean value, not OK

- Quay lại add()ví dụ về hàm, bạn có thể thay đổi kiểu của các tham số từ anyunion thành union như sau:
function add(a: number | string, b: number | string) {
    if (typeof a === 'number' && typeof b === 'number') {
        return a + b;
    }
    if (typeof a === 'string' && typeof b === 'string') {
        return a.concat(b);
    }
    throw new Error('Parameters must be numbers or strings');
}