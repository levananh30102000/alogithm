1.Giới thiệu
- Một enum là một nhóm các giá trị hằng số được đặt tên.Enum là viết tắt của kiểu liệt kê.
enum name { constant1, constant2, ... };

- Để xác định một enum, bạn làm theo các bước sau:
+ Đầu tiên, sử dụng enumtừ khóa theo sau là tên của enum.
+ Sau đó, xác định các giá trị không đổi cho enum.

2.Ví dụ
- Ví dụ sau tạo một enum đại diện cho các tháng trong năm:
    enum Month {
        Jan,
        Feb,
        Mar,
        Apr,
        May,
        Jun,
        Jul,
        Aug,
        Sep,
        Oct,
        Nov,
        Dec
};
- Phần sau khai báo một hàm sử dụng Monthenum làm kiểu monththam số:
function isItSummer(month: Month) {
    let isSummer: boolean;
    switch (month) {
        case Month.Jun:
        case Month.Jul:
        case Month.Aug:
            isSummer = true;
            break;
        default:
            isSummer = false;
            break;
    }
    return isSummer;
}

3.Cách hoạt động
    - ví dụ sau chuyển một số thay vì một enum vào isItSummer()hàm.Và nó hoạt động.
    console.log(isItSummer(6)); // true
- Hãy kiểm tra mã Javascript được tạo của tháng enum:
var Month;
(function (Month) {
    Month[Month["Jan"] = 0] = "Jan";
    Month[Month["Feb"] = 1] = "Feb";
    Month[Month["Mar"] = 2] = "Mar";
    Month[Month["Apr"] = 3] = "Apr";
    Month[Month["May"] = 4] = "May";
    Month[Month["Jun"] = 5] = "Jun";
    Month[Month["Jul"] = 6] = "Jul";
    Month[Month["Aug"] = 7] = "Aug";
    Month[Month["Sep"] = 8] = "Sep";
    Month[Month["Oct"] = 9] = "Oct";
    Month[Month["Nov"] = 10] = "Nov";
    Month[Month["Dec"] = 11] = "Dec";
})(Month || (Month = {}));
- Và bạn có thể xuất Monthbiến ra bảng điều khiển:
{
    '0': 'Jan', 
    '1': 'Feb', 
    '2': 'Mar', 
    '3': 'Apr', 
    '4': 'May', 
    '5': 'Jun', 
    '6': 'Jul', 
    '7': 'Aug', 
    '8': 'Sep', 
    '9': 'Oct', 
    '10': 'Nov',
    '11': 'Dec',
    Jan: 0,     
    Feb: 1,     
    Mar: 2,     
    Apr: 3,     
    May: 4,
    Jun: 5,
    Jul: 6,
    Aug: 7,
    Sep: 8,
    Oct: 9,
    Nov: 10,
    Dec: 11
}
  
4. Chỉ định số thành viên
- TypeScript xác định giá trị số của thành viên enum dựa trên thứ tự của thành viên đó xuất hiện trong định nghĩa enum.
enum Month {
    Jan = 1,
    Feb,
    Mar,
    Apr,
    May,
    Jun,
    Jul,
    Aug,
    Sep,
    Oct,
    Nov,
    Dec
};

- Bạn nên sử dụng một enum khi bạn:
+ Có một tập hợp nhỏ các giá trị cố định có liên quan chặt chẽ với nhau
+ Và những giá trị này đã được biết tại thời điểm biên dịch.

