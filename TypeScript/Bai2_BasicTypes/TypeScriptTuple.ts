- Một bộ tuple hoạt động giống như một mảng với một số cân nhắc bổ sung:
+ Số lượng phần tử trong bộ được cố định.
+ Các loại phần tử đã biết và không cần phải giống nhau.
    
- Ví dụ: bạn có thể sử dụng một bộ giá trị để biểu thị một giá trị dưới dạng một cặp a stringvà a number:
let skill: [string, number];
skill = ['Programming', 5];

- Kể từ TypeScript 3.0, một tuple có thể có các phần tử tùy chọn được chỉ định bằng cách sử dụng hậu tố dấu hỏi(?).
+ Kể từ TypeScript 3.0, một tuple có thể có các phần tử tùy chọn được chỉ định bằng cách sử dụng hậu tố dấu hỏi(?):
let bgColor, headerColor: [number, number, number, number?];
bgColor = [0, 255, 255, 0.5];
headerColor = [0, 255, 255];