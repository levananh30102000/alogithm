- Loại bí danh cho phép bạn tạo tên mới cho một loại hiện có.Sau đây là cú pháp của kiểu bí danh:
type alias = existingType;
- Ví dụ sau sử dụng các ký tự bí danh kiểu cho kiểu chuỗi:
type chars = string;
let messsage: chars; // same as string type
-  tạo bí danh kiểu cho các kiểu liên hợp.Ví dụ:
type alphanumeric = string | number;
let input: alphanumeric;
input = 100; // valid
input = 'Hi'; // valid
input = false; // Compiler error