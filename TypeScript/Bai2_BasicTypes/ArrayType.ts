1. Giới thiệu
- TypeScript arraylà một danh sách dữ liệu có thứ tự.
let arrayName: type[];
- Và bạn có thể thêm một hoặc nhiều chuỗi vào mảng:
skills[0] = "Problem Solving";
skills[1] = "Programming";
- hoặc sử dụng push()phương pháp:
skills.push('Software Design');
- Phần sau khai báo một biến và gán một mảng chuỗi cho nó:
let skills = ['Problem Sovling', 'Software Design', 'Programming'];

2. Thuộc tính và phương thức
- thuộc tính length để lấy số phần tử trong một mảng:
let series = [1, 2, 3];
console.log(series.length); // 3

3. Lưu trữ giá trị
 - Phần sau minh họa cách khai báo một mảng chứa cả chuỗi và số:
let scores = ['Programming', 5, 'Software Design', 4]; 
- TypeScript suy diễn scoresmảng là một mảng của string | number.
let scores : (string | number)[];
scores = ['Programming', 5, 'Software Design', 4]; 
