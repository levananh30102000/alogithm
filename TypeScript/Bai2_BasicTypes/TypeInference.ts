1. Suy luận cơ bản
- Khi bạn khai báo một biến, bạn có thể sử dụng chú thích kiểu để chỉ định rõ ràng kiểu cho nó.Ví dụ:
let counter: number;
- Tương tự như vậy, khi bạn gán một giá trị cho tham số hàm, TypeScript suy ra kiểu của tham số thành kiểu của giá trị mặc định.Ví dụ:
function setCounter(max=100) {
    // ...
}

2. Thuật toán loại chung tốt nhất
let items = [1, 2, 3, null];
- Để suy ra kiểu của itemsbiến, TypeScript cần xem xét kiểu của từng phần tử trong mảng.
- Nó sử dụng thuật toán loại chung tốt nhất để phân tích từng loại ứng viên và chọn loại tương thích với tất cả các ứng viên khác.

3. Nhập theo ngữ cảnh
- TypeScript sử dụng vị trí của các biến để suy ra kiểu của chúng.Cơ chế này được gọi là nhập theo ngữ cảnh.Ví dụ:
document.addEventListener('click', function (event) {
    console.log(event.button); // 
});

- Trong thực tế, bạn nên luôn sử dụng kiểu suy luận càng nhiều càng tốt. Và bạn sử dụng chú thích kiểu trong các trường hợp sau:
+ Khi bạn khai báo một biến và gán giá trị cho nó sau đó.
+ Khi bạn muốn một biến không thể suy ra được.
+ Khi một hàm trả về anykiểu và bạn cần làm rõ giá trị.