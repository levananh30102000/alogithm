// Lớp trong TypeScript thêm các chú thích kiểu vào các thuộc tính và phương thức của lớp. 
// Phần sau hiển thị Personlớp trong TypeScript:
class Person {
    ssn: number;
    firstName: string;
    lastName: string;

    constructor(ssn: number, firstName: string, lastName: string) {
        this.ssn = ssn;
        this.firstName = firstName;
        this.lastName = lastName;
    }

    getFullName(): string {
        return `${this.firstName} ${this.lastName}`;
    }
}
let person = new Person(171280926, 'John', 'Doe');