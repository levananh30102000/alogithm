// Interface TypeScript xác định các hợp đồng trong mã của bạn.
// Họ cũng cung cấp tên rõ ràng để kiểm tra loại.
interface Person {
    firstName: string;
    lastName: string;
}
function getFullName(person: Person) {
    return `${person.firstName} ${person.lastName}`;
}

let Dinh = {
    firstName: 'Dinh',
    lastName: 'Ta Van'
};

console.log(getFullName(Dinh));

// THUỘC TÍNH TÙY CHỌN
/**
 *  Một giao diện có thể có các thuộc tính tùy chọn.
 *  Để khai báo một thuộc tính tùy chọn, bạn sử dụng dấu chấm hỏi (?)ở cuối tên thuộc tính trong khai báo
 */
 interface Person2 {
    firstName: string;
    middleName?: string;
    lastName: string;
}
function getFullName_2(person: Person2) {
    if (person.middleName) {
        return `${person.firstName} ${person.middleName} ${person.lastName}`;
    }
    return `${person.firstName} ${person.lastName}`;
}

// CÁC LOẠI CHỨC NĂNG
// Ngoài việc mô tả một đối tượng với các thuộc tính, các interface cũng cho phép bạn mô tả các kiểu chức năng .
interface StringFormat {
    (str: string, isUpper: boolean): string
}
let format: StringFormat;

format = function (str: string, isUpper: boolean) {
    return isUpper ? str.toLocaleUpperCase() : str.toLocaleLowerCase();
};

console.log(format('hi', true));


// CÁC LOẠI LỚP
// Interface Json sau có thể được triển khai bởi bất kỳ lớp nào không liên quan:
interface Json {
    toJson(): string
}
class Person4 implements Json {
    constructor(private firstName: string,
        private lastName: string) {
    }
    toJson(): string {
        return JSON.stringify(this);
    }
}
let person4 = new Person4('John', 'Doe');
console.log(person4.toJson());
