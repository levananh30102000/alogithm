// Element & Component Access

// Accessing the Root Instance
// The root Vue instance
new Vue({
    data: {
      foo: 1
    },
    computed: {
      bar: function () { /* ... */ }
    },
    methods: {
      baz: function () { /* ... */ }
    }
  })

  // Get root data
this.$root.foo

// Set root data
this.$root.foo = 2

// Access root computed properties
this.$root.bar

// Call root methods
this.$root.baz()

// Accessing the Parent Component Instance
var map = this.$parent.map || this.$parent.$parent.map

// Accessing Child Component Instances & Child Elements
this.$refs.usernameInput

// methods: {
//     // Used to focus the input from the parent
//     focus: function () {
//       this.$refs.input.focus()
//     }
//   }

this.$refs.usernameInput.focus()

// Dependency Injection
var googleMap = new Vue({
    el: '#ggmap',
    inject: ['getMap'],
    methods : {
        provide: function () {
            return {
            getMap: this.getMap
            }
        }
    }
})

//  Recursive Components 
Vue.component('unique-name-of-my-component', {
    name: 'unique-name-of-my-component',
    name: 'stack-overflow',
    template: '<div><stack-overflow></stack-overflow></div>'
  })

//   Circular References Between Components
components: {
    TreeFolderContents: () => import('./tree-folder-contents.vue')
  }

//   X-Templates
Vue.component('hello-world', {
    template: '#hello-world-template'
  })

// Controlling Updates
// Cheap Static Components with v-once
Vue.component('terms-of-service', {
    template: `
      <div v-once>
        <h1>Terms of Service</h1>
        ... a lot of static content ...
      </div>
    `
  })

