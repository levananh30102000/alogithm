// Prop Casing (camelCase vs kebab-case)

Vue.component('blog-post', {
    // camelCase in JavaScript
    props: ['postTitle'],
    template: '<h3>{{ postTitle }}</h3>'
  })

//   Các loại đề xuất

// props: ['title', 'likes', 'isPublished', 'commentIds', 'author']

// props: {
//     title: String,
//     likes: Number,
//     isPublished: Boolean,
//     commentIds: Array,
//     author: Object,
//     callback: Function,
//     contactsPromise: Promise // or any other constructor
//   }


// Truyền một object
// post: {
//     id: 1,
//     title: 'My Journey with Vue'
//   }

// Prop Validation

Vue.component('my-component', {
    props: {
      // Basic type check (`null` and `undefined` values will pass any type validation)
      propA: Number,
      // Multiple possible types
      propB: [String, Number],
      // Required string
      propC: {
        type: String,
        required: true
      },
      // Number with a default value
      propD: {
        type: Number,
        default: 100
      },
      // Object with a default value
      propE: {
        type: Object,
        // Object or array defaults must be returned from
        // a factory function
        default: function () {
          return { message: 'hello' }
        }
      },
      // Custom validator function
      propF: {
        validator: function (value) {
          // The value must match one of these strings
          return ['success', 'warning', 'danger'].includes(value)
        }
      }
    }
  })

//   Type Checks

// Vue.component('blog-post', {
//     props: {
//       author: Person
//     }
//   })

//   Disabling Attribute Inheritance 
Vue.component('base-input', {
    inheritAttrs: false,
    props: ['label', 'value'],
    template: `
      <label>
        {{ label }}
        <input
          v-bind="$attrs"
          v-bind:value="value"
          v-on:input="$emit('input', $event.target.value)"
        >
      </label>
    `
  })

//   


