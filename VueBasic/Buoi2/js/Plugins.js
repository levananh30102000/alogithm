MyPlugin.install = function (Vue, options) {
    // 1. Thêm phương thức hoặc thuộc tính cấp toàn cục
    Vue.myGlobalMethod = function () {
    }
  
    // 2. Thêm một directive cấp toàn cục
    Vue.directive('my-directive', {
      bind (el, binding, vnode, oldVnode) {
      }
    })
  
    // 3. Thêm một số tùy chọn cho component
    Vue.mixin({
      created: function () {
      }
    })
  
    // 4. Thêm một phương thức đối tượng
    Vue.prototype.$myMethod = function (methodOptions) {
    }
  }

//   Sử dụng plugin

// Khi dùng CommonJS với Browserify hoặc Webpack…
var Vue = require('vue')
var VueRouter = require('vue-router')

// …đừng quên gọi Vue.use()
Vue.use(VueRouter)

