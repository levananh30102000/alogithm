// Tên Component
Vue.component('my-component-name', { /* ... */ })

// Quy tắc đặt tên
// Dùng kebab-case

Vue.component('my-component-name', { /* ... */ })

// Dùng PascalCase

Vue.component('MyComponentName', { /* ... */ })

// Đăng ký ở cấp toàn cục

Vue.component('component-a', { /* ... */ })
Vue.component('component-b', { /* ... */ })
Vue.component('component-c', { /* ... */ })

new Vue({ el: '#app' })

//Đăng ký ở cấp cục bộ
var ComponentA = { /* ... */ }
var ComponentB = { /* ... */ }
var ComponentC = { /* ... */ }

new Vue({
    el: '#app',
    components: {
      'component-a': ComponentA,
      'component-b': ComponentB
    }
  })

//   Hệ thống module
//   Đăng kí ở cấp cục bộ trong hệ thống Module
// import ComponentA from './ComponentA'
// import ComponentC from './ComponentC'

// export default {
//   components: {
//     ComponentA,
//     ComponentC
//   },
//   // ...
// }


// // Tự động đăng kí toàn cục của những component cơ sở
import BaseButton from './BaseButton.vue'
import BaseIcon from './BaseIcon.vue'
import BaseInput from './BaseInput.vue'

export default {
  components: {
    BaseButton,
    BaseIcon,
    BaseInput
  }
}


// 
import Vue from 'vue'
import upperFirst from 'lodash/upperFirst'
import camelCase from 'lodash/camelCase'

const requireComponent = require.context(
  // Đường dẫn tương đối của thư mục component
  './components',
  // có tìm component trong các thư mục con hay không
  false,
  // regular expression để tìm các file component cơ sở
  /Base[A-Z]\w+\.(vue|js)$/
)

requireComponent.keys().forEach(fileName => {
  // Lấy cấu hình của component
  const componentConfig = requireComponent(fileName)

  // Lấy tên của component dùng PascalCase
  const componentName = upperFirst(
    camelCase(
      // Bỏ phần đầu `'./` và đuôi file
      fileName.replace(/^\.\/(.*)\.\w+$/, '$1')
    )
  )

  // Khai báo component cấp toàn cục
  Vue.component(
    componentName,
    // Tìm kiếm các tùy chọn của component trong thuộc tính `.default`
    // Thuộc tính này sẽ khả dụng nếu component sử dụng `export default`
    // nếu không thì dùng chính `componentConfig`
    componentConfig.default || componentConfig
  )
})