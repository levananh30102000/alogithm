// Định nghĩa một component với tên là "button-counter"
Vue.component('button-counter', {
  data: function () {
    return {
      count: 0
    }
  },
  template: '<button v-on:click="count++">Bạn đã bấm {{ count }} lần.</button>'
})
new Vue({ el: '#components-demo' })
//   Truyền dữ liệu component con bằng POP

Vue.component('blog-post', {
    props: ['title'],
    template: '<h3>{{ title }}</h3>'
})

new Vue({
    el: '#blog-post',
    data: {
        posts: [
            { id : 1, title: 'Giới thiệu về vue'},
            { id : 2, title: 'Các khái niệm trong Vue'},
            { id : 3, title: 'Vue căn bản và vô cùng nâng cao'},
        ]
    }
})

// Gửi thông tin đến đối tượng cha bằng sự kiện  
Vue.component('blog-post', {
  props: ['post'],
  template: `
    <div class="blog-post">
      <h3>{{ post.title }}</h3>
      <button>
        Phóng to
      </button>
      <div v-html="post.content"></div>
    </div>
  `
})
new Vue({
  el: '#blog-posts-events-demo',
  data: {
    posts: [
    { id : 1, title: 'Giới thiệu về vue'},
    { id : 2, title: 'Các khái niệm trong Vue'},
    { id : 3, title: 'Vue căn bản và vô cùng nâng cao'}
  ],
    postFontSize: 1
  }, 
    methods: {
      onEnlargeText: function (enlargeAmount) {
        this.postFontSize += enlargeAmount
      }
    },
})

// V-model với component
Vue.component('custom-input', {
  props: ['value'],
  template: `
    <input
      v-bind:value="value"
      v-on:input="$emit('input', $event.target.value)
    >
  `
})


