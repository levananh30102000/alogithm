
// định nghĩa một object mixin
var myMixin = {
    created: function () {
      this.hello()
    },
    methods: {
      hello: function () {
        console.log('Mixin xin chào!')
      }
    }
  }
  
  // định nghĩa một component sử dụng mixin này
  var Component = Vue.extend({
    mixins: [myMixin]
  })
  
  var component = new Component() // => "Mixin xin chào!"

//   Hợp nhất các tùy chọn

var mixin = {
    methods: {
      foo: function () {
        console.log('foo')
      },
      hàmTrùngTên: function () {
        console.log('từ mixin')
      }
    }
  }
  
  var vm = new Vue({
    mixins: [mixin],
    methods: {
      bar: function () {
        console.log('bar')
      },
      hàmTrùngTên: function () {
        console.log('từ component')
      }
    }
  })
  
  vm.foo() // => "foo"
  vm.bar() // => "bar"
  vm.hàmTrùngTên() // => "từ component"
  
//   Mixin toàn cục

// chèn một handler cho option tùy biến `myOption`
Vue.mixin({
    created: function () {
      var myOption = this.$options.myOption
      if (myOption) {
        console.log(myOption)
      }
    }
  })
  
  new Vue({
    myOption: 'hello!'
  })
  // => "hello!"

//   Chiến lược merge tùy chọn tùy biến
const merge = Vue.config.optionMergeStrategies.computed
Vue.config.optionMergeStrategies.vuex = function (toVal, fromVal) {
  if (!toVal) return fromVal
  if (!fromVal) return toVal
  return {
    getters: merge(toVal.getters, fromVal.getters),
    state: merge(toVal.state, fromVal.state),
    actions: merge(toVal.actions, fromVal.actions)
  }
}

