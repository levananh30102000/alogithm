new Vue({
    el: '#example',
    data: {
        message: 'người đông bến đợi thuyền xuôi ngược'
    },
    // computed: {
    //     // một computed getter
    //     reversedMessage: function () {
    //     // `this` trỏ tới đối tượng vm
    //     return this.message.split(' ').reverse().join(' ')
    //   }
    // }
    // trong component
    methods: {
    reverseMessage: function () {
      return this.message.split(' ').reverse().join(' ')
    }
  }
});

new Vue({
    el: '#demo',
    data: {
        firstName: 'Evan',
        lastName: 'You'
    },
    computed: {
        fullName: function () {
            return this.firstName + ' ' + this.lastName
        }
    }
});
