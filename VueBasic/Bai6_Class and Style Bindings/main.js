new vue({
    el: '#abc',
    data: {
        isActive: true,
        error: null
      },
      
      computed: {
        classObject: function () {
          return {
            active: this.isActive && !this.error,
            'text-danger': this.error && this.error.type === 'fatal'
          }
        }
      }
})

// Các lớp HTML ràng buộc
// Cú pháp mảng
data: {
  activeClass: 'active',
  errorClass: 'text-danger'
}

// Ràng buộc các kiểu nội tuyến
// Cú pháp đối tượng
data: {
  activeColor: 'red',
  fontSize: 30
}
